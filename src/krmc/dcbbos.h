!
!
!     Real or approximate boson symmetry of orbitals.
!     
!     Depends on maxorb.h
!
      INTEGER IBOSYM(MXCORB)
      COMMON /DCIBOS/ IBOSYM
