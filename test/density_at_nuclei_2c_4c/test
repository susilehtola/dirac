#!/usr/bin/env python

import os
import sys
import shutil

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from runtest_dirac import Filter, TestRun

test = TestRun(__file__, sys.argv)

f = Filter()
f.add(from_string = 'Rho at nuc S  01',
      num_lines   = 7,
      rel_tolerance   = 1.0e-5)

test.run(['atoms.inp'], ['atoms.xyz'],
                 args='--replace closed_occ="4" --replace open_e="5" --replace open_orb="6" --replace myatom="F" --get="aoo2esoc.009"')

test.run(['atoms.inp'], ['atoms.xyz'],
                 args='--replace closed_occ="12" --replace open_e="4" --replace open_orb="6" --replace myatom="S" --get="aoo2esoc.016"')

# 4c reference calculation
test.run(['4c.inp'], ['sf6.xyz'], f)

# molecular U matrix (from molecular core h1^{4c}) + AMFI 2e-SO corrections
test.run(['amfi.inp'], ['sf6.xyz'], f)

# molecular U matrix (from molecular core h1^{4c}) + X-AMFI 2e-SO corrections
test.run(['xamfi.inp'], ['sf6.xyz'], f, args='--put="aoo2esoc*"')

os.unlink('aoo2esoc.009')
os.unlink('aoo2esoc.016')

sys.exit(test.return_code)
